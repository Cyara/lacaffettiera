from django.db import models

# Create your models here.
class Link(models.Model):
	key = models.SlugField(unique=True, max_length=100, verbose_name='Nombre clave')
	name = models.CharField(max_length=100, verbose_name='Red social')
	url = models.URLField(max_length=200, null=True, blank=True, verbose_name='Enlace')
	created = models.DateTimeField(auto_now_add=True, verbose_name='Creación')
	updated = models.DateTimeField(auto_now=True, verbose_name='Actualización')

	class Meta:
		verbose_name = 'Enlace'
		verbose_name_plural = 'Enlaces'
		ordering = ['name']

	def __str__(self):
		return self.name
		