# La Caffettiera

Proyecto realizado en Django y que se hace uso de los template tag, la plantilla hace uso de Bootstrap y JavaScript y como bases de datos SQLite.

Un proyecto funcional con servicio para gestionar usuarios, blog para publicación de posts estableciendo categorías, detalles entre otros, presentación de servicios entre otros.


### Ejecutar proyecto

```
$ python manage.py makemigrations
```

```
$ python manage.py migrate
```

```
$ python manage.py runserver
```

### Crear usuarios o superususarios

```
$ python manage.py createsuperuser
```
```
$ python manage.py createuser
```

# Lista de rutas

| Sección  |  Ruta |
| ------------ | ------------ |
| Admin | admin/ |
| Core | / |
| Core | /store/ |
| Core | /about/ |
| Contact | /contact/ |
| Blog | /blog/ |
| Blog | /blog/category/< int:category_id >/ |
| Services | /services/ |
| Pages | < int:page_id>/< slug:page_slug >/ |


# Contacto

| Nombre  |  Email |
| ------------ | ------------ |
|  Cristhian Yara |  cmyarap@gmail.com |