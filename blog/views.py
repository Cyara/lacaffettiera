from django.shortcuts import render, get_object_or_404
from .models import Post, Category
# Create your views here.

def blog(request):
	post = Post.objects.all()
	context = {'post': post}
	return render(request, 'blog/blog.html', context)

# Método 1
'''
def category(request, category_id):
	category = get_object_or_404(Category,id=category_id)
	#category = Category.object.get(id=category_id)
	post = Post.objects.filter(categories=category)
	context = {'category': category, 'post':post}
	return render(request, 'blog/category.html', context)
'''
# Método 2
def category(request, category_id):
	category = get_object_or_404(Category,id=category_id)
	context = {'category': category}
	return render(request, 'blog/category.html', context)