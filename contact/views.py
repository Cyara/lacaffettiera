from django.shortcuts import render, redirect
from django.urls import reverse
from django.core.mail import EmailMessage
from .forms import ContactForm

# Create your views here.
def contact(request):
	contact_form = ContactForm()
	context = {'contact_form':contact_form}

	if request.method == 'POST':
		contact_form = ContactForm(data = request.POST)
		if contact_form.is_valid():
			name = request.POST.get('name', '')
			email = request.POST.get('email', '')
			content = request.POST.get('content', '')
			# Enviamos el correo y redireccionamos
			'''
				asunto,
				cuerpo,
				email_origen,
				email_destino,
				reply_to = [email]
			'''
			email = EmailMessage(
				'La Caffettiera: Nuevo mensaje de contacto',
				'De {} <{}>\n\nEscribio: {}'.format(name,email,content),
				'no-contestar@inbox.mailtrap.io',
				['cmyarap@gmail.com'],
				reply_to = [email]
			)
			try:
				email.send()
				#suponiendo que todo va bien, redireccionamos
				return redirect(reverse('contact')+'?ok')
			except: 
				#Algo no ha ido bien, redireccionamos a fail
				return redirect(reverse('contact')+'?fail')

	return render(request, 'contact/contact.html', context)